<?php

// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// * Copyright 2014 The Herosphp Authors. All rights reserved.
// * Use of this source code is governed by a MIT-style license
// * that can be found in the LICENSE file.
// * +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

declare(strict_types=1);

namespace herosphp\plugin\storage\core;

/**
 * 文件上传错误代码
 * ---------------------------------------------------
 * @author RockYang<yangjian102621@gmail.com>
 */
enum UploadError: int
{
    case NO_FILE_UPLOAD = 0;
    case FILESIZE_OVER_LIMIT = 1;
    case EXT_NOT_ALLOW = 2;
    case SAVE_FILE_FAIL = 3;
    case PART_UPLOADED = 4;
    case IMG_DECODE_FAIL = 5;

    public function getMessage(): string
    {
        return match ($this) {
            self::NO_FILE_UPLOAD => '没有上传任何文件',
            self::FILESIZE_OVER_LIMIT => '文件超出大小限制',
            self::EXT_NOT_ALLOW => '非法的文件后缀名',
            self::SAVE_FILE_FAIL => '保存上传文件失败',
            self::PART_UPLOADED => '部分文件上传成功',
            self::IMG_DECODE_FAIL => '图片数据解码失败',
        };
    }
}
